﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactMe.Data
{
    public class DetailTypesRepository : IDetailTypesRepository
    {
        private ContactMeModelContainer _ctx;
        public DetailTypesRepository()
        {
            _ctx = new ContactMeModelContainer();
        }

        public bool Save()
        {
            _ctx.SaveChanges();
            return true;
        }

        public bool SaveDetailType(DetailType type, out DetailType savedType)
        {
            savedType = null;
            var old = _ctx.DetailTypes.Where(a=>a.Id == type.Id).FirstOrDefault();
            if (old == null)
            {
                foreach (var contact in _ctx.Contacts)
                    contact.Details.Add(new Detail { Value="", Contact=contact, DetailType=type});
                savedType = _ctx.DetailTypes.Add(type);
            }
            else
            {
                old.Name = type.Name;
                old.Placeholder = type.Placeholder;
                old.Required = type.Required;
                savedType = old;
            }
            return true;
        }

        public bool DeleteDetailType(int id)
        {
            var type = _ctx.DetailTypes.Where(a=>a.Id == id).FirstOrDefault();
            if (type == null)
                return false;
            else
                _ctx.DetailTypes.Remove(type);
            return true;
        }

        public IEnumerable<DetailType> GetDetailTypes()
        {
            return _ctx.DetailTypes;
        }
    }
}