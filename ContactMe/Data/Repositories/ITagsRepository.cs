﻿using ContactMe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContactMe.Data
{
    public interface ITagsRepository
    {
        bool Save();
        IEnumerable<Tag> GetTags();
    }
}
