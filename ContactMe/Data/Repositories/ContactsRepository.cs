﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContactMe.Data
{
    public class ContactsRepository : IContactsRepository
    {
        private ContactMeModelContainer _ctx;
        public ContactsRepository()
        {
            _ctx = new ContactMeModelContainer();
        }

        public bool Save()
        {
            _ctx.SaveChanges();
            return true;
        }

        public IEnumerable<Contact> GetAllContacts()
        {
            var contacts = _ctx.Contacts
                .Include("Details")
                .Include("Phones")
                .Include("Emails")
                .Include("Tags")
                .AsEnumerable();
            return contacts;
        }

        public bool AddContact(Contact contact)
        {
            var details = contact.Details;
            var phones = contact.Phones;
            var emails = contact.Emails;
            var tags = contact.Tags;

            contact.Details = null;
            contact.Phones = null;
            contact.Emails = null;
            contact.Tags = null;

            _ctx.Contacts.Add(contact);
            foreach (var detail in details)
            {
                detail.Contact = contact;
                var dt = _ctx.DetailTypes.Where(a => a.Id == detail.DetailType.Id).FirstOrDefault();
                detail.DetailType = dt;
                dt.Details.Add(detail);
            }

            foreach (var phone in phones)
                if (!string.IsNullOrEmpty(phone.Number))
                {
                    phone.Contact = contact;
                    _ctx.Phones.Add(phone);
                }
            foreach (var email in emails)
                if (!string.IsNullOrEmpty(email.Address))
                {
                    email.Contact = contact;
                    _ctx.Emails.Add(email);
                }
            foreach (var tag in tags)
                if (!string.IsNullOrEmpty(tag.Value))
                {
                    tag.Contact = contact;
                    _ctx.Tags.Add(tag);
                } 
            return true;
        }

        public bool SaveContact(Contact contact)
        {
            var old = _ctx.Contacts
                .Include("Details")
                .Include("Phones")
                .Include("Emails")
                .Include("Tags")
                .Where(a => a.Id == contact.Id).FirstOrDefault();

            //list of phones and emails to be removed 
            //because they don't exist in the new contact but do in the old contact
            var removedPhones = new List<Phone>();
            var removedEmails = new List<Email>();
            var removedTags = new List<Tag>();

            //collection of phones and emails to be added 
            //because they exit in the new contact but don't in the old contact
            //add everything then remove later if it exists in both old and new contact
            var addedPhones = contact.Phones;
            var addedEmails = contact.Emails;
            var addedTags = contact.Tags;


            foreach (var oldDetail in old.Details)
            {
                var newDetail = contact.Details.Where(a => a.Id == oldDetail.Id).FirstOrDefault();
                _ctx.Entry(oldDetail).CurrentValues.SetValues(newDetail);
            }
           
            foreach (var oldPhone in old.Phones)
            {
                var newPhone = contact.Phones.Where(a => a.Id == oldPhone.Id).FirstOrDefault();
                if (newPhone == null || string.IsNullOrEmpty(newPhone.Number))
                {
                    //phone has been removed
                    //add to list so it can be removed later 
                    //without affecting the foreach loop
                    removedPhones.Add(oldPhone);
                }
                else
                {
                    //update the value/s in oldPhone
                    _ctx.Entry(oldPhone).CurrentValues.SetValues(newPhone);

                    //remove because it exists in both old and new contact
                    addedPhones.Remove(newPhone);
                }
            }
            foreach (var oldPhone in removedPhones)
                _ctx.Phones.Remove(oldPhone);
            foreach (var newPhone in addedPhones)
                if (!string.IsNullOrEmpty(newPhone.Number))
                    old.Phones.Add(newPhone); 
            
            foreach (var oldEmail in old.Emails)
            {
                var newEmail = contact.Emails.Where(a => a.Id == oldEmail.Id).FirstOrDefault();
                if (newEmail == null || string.IsNullOrEmpty(newEmail.Address))
                {
                    //email has been removed
                    //add to list so it can be removed later 
                    //without affecting the foreach loop
                    removedEmails.Add(oldEmail);
                }
                else
                {
                    //update the value/s in oldEmail
                    _ctx.Entry(oldEmail).CurrentValues.SetValues(newEmail);

                    //remove because it exists in both old and new contact
                    addedEmails.Remove(newEmail);
                }
            }
            foreach (var oldEmail in removedEmails)
                _ctx.Emails.Remove(oldEmail);
            foreach (var newEmail in addedEmails)
                if(!string.IsNullOrEmpty(newEmail.Address))
                    old.Emails.Add(newEmail);

            foreach (var oldTag in old.Tags)
            {
                var newTag = contact.Tags.Where(a => a.Id == oldTag.Id).FirstOrDefault();
                if (newTag == null || string.IsNullOrEmpty(newTag.Value))
                {
                    //tag has been removed
                    //add to list so it can be removed later 
                    //without affecting the foreach loop
                    removedTags.Add(oldTag);
                }
                else
                {
                    //update the value/s in oldTag
                    _ctx.Entry(oldTag).CurrentValues.SetValues(newTag);

                    //remove because it exists in both old and new contact
                    addedTags.Remove(newTag);
                }
            }
            foreach (var oldTag in removedTags)
                _ctx.Tags.Remove(oldTag);
            foreach (var newTag in addedTags)
                if (!string.IsNullOrEmpty(newTag.Value))
                    old.Tags.Add(newTag);
            
            _ctx.Entry(old).CurrentValues.SetValues(contact);
            return true;
        }

        public bool DeleteContact(int id)
        {
            var contact = _ctx.Contacts.Where(a=>a.Id==id).FirstOrDefault();
            if (contact != null)
            {
                _ctx.Contacts.Remove(contact);
                return true;
            }
            else
                return false;
        }

        public Contact GetContact(int id)
        {
            return _ctx.Contacts
                .Include("Details")
                .Include("Details.DetailType")
                .Include("Phones")
                .Include("Emails")
                .Include("Tags")
                .Where(a => a.Id == id).FirstOrDefault();
        }

        public IEnumerable<Contact> SearchContacts(IEnumerable<string> keywords)
        {
            return _ctx.Contacts.Include("Details").Include("Emails").Include("Phones").Include("Tags").Where(
                c => keywords.Any(
                k => c.Details.Any(d=>d.Value.Contains(k) || 
                     c.Emails.Any(e=>e.Address.Contains(k)) || 
                     c.Phones.Any(p=>p.Number.Contains(k)) || 
                     c.Tags.Any(t=>t.Value.Contains(k)))));
        }
    }
}
