﻿using ContactMe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContactMe.Data
{
    public interface IContactsRepository
    {
        bool Save();

        IEnumerable<Contact> SearchContacts(IEnumerable<string> keywords);
        IEnumerable<Contact> GetAllContacts();
        Contact GetContact(int id);
        bool AddContact(Contact contact);
        bool SaveContact(Contact contact);
        bool DeleteContact(int id);
    }
}
