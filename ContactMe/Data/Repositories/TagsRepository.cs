﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactMe.Data
{
    public class TagsRepository : ITagsRepository
    {
        private ContactMeModelContainer _ctx;
        public TagsRepository()
        {
            _ctx = new ContactMeModelContainer();
        }

        public bool Save()
        {
            _ctx.SaveChanges();
            return true;
        }

        public IEnumerable<Tag> GetTags()
        {
            return _ctx.Tags.GroupBy(a => a.Value).Select(t=>t.FirstOrDefault());
        }
    }
}