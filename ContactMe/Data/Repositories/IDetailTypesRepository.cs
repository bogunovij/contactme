﻿using ContactMe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContactMe.Data
{
    public interface IDetailTypesRepository
    {
        bool Save();
        bool SaveDetailType(DetailType type, out DetailType savedType);
        bool DeleteDetailType(int id);
        IEnumerable<DetailType> GetDetailTypes();
    }
}
