﻿
var addContactController = ["$scope", "$http", "$window", "tagsService", "detailTypesService", "contactsService", function ($scope, $http, $window, tagsService, detailTypesService, contactsService) {
    
    $scope.detailTypes = detailTypesService.detailTypes;
    var tag = {
        value: "", inputChanged: function (str) {
            tag.value = str;
        }
    };
    $scope.contact = {
        details: [], phones: [{ number: "", type: "Home" }], emails: [{ address: "" }], tags: [tag]
    };
    detailTypesService.getDetailTypes().then(function () {
        for (var i = 0; i < $scope.detailTypes.length; i++) {
            $scope.contact.details.push({value: "", detailType: $scope.detailTypes[i]});
        }
        $scope.detailTypesLoaded = true;
    }, function () {
        alert('error');
    });
    $scope.tags = tagsService.tags;
    tagsService.getTags().then(function () {
        $scope.tagsLoaded = true;
    }, function () {
        alert('error');
    });
    $scope.addContact = function (contact) {
        var found = false;
        for (var i = 0; i < $scope.contact.details.length; i++) {
            var detail = $scope.contact.details[i];
            if (detail.detailType.required && detail.value == "") {
                found = true;
            }
        }
        if (!found) {
            for (var i = 0; i < $scope.contact.tags.length; i++) {
                var tag = $scope.contact.tags[i];
                if (tag.obj && tag.obj.title.length > 0)
                    tag.value = tag.obj.title;
            }
            $scope.busy = true;
            contactsService.addContact(contact).then(function () {
                $scope.busy = false;
                $window.location = "#/contacts";
            }, function () {
                alert('An error has occurred!');
                $scope.busy = false;
            });
        }
        else
            $scope.showRequired = true;
    };
    $scope.addPhone = function () {
        var phone = {number: "", type: "Home"};
        $scope.contact.phones.push(phone);
    };
    $scope.removePhone = function (index) {
        $scope.contact.phones.splice(index, 1);
    };
    $scope.addEmail = function () {
        var email = {address: "" };
        $scope.contact.emails.push(email);
    };
    $scope.removeEmail = function (index) {
        $scope.contact.emails.splice(index, 1);
    };
    $scope.addTag = function ()
    {
        var tag = {
            value: "", inputChanged: function (str) {
                tag.value = str;
            }
        };
        $scope.contact.tags.push(tag);
    };
    $scope.removeTag = function (index) {
        $scope.contact.tags.splice(index, 1);
    };

    $scope.loading = function(){
        return !($scope.detailTypesLoaded && $scope.tagsLoaded) || $scope.busy;
    };
}];