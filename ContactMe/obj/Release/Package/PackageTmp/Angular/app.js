﻿var app = angular.module("app", ['ngRoute', 'angucomplete-alt']);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when("/",
        {
            controller: "homeController",
            templateUrl: "/Angular/Views/home.html"
        }).when("/contacts",
        {
            controller: "contactsController",
            templateUrl: "/Angular/Views/contacts.html"
        }).when("/contacts/:id",
        {
            controller: "contactController",
            templateUrl: "/Angular/Views/contact.html"
        }).when("/addContact",
        {
            controller: "addContactController",
            templateUrl: "/Angular/Views/addContact.html"
        }).when("/manage",
        {
            controller: "manageController",
            templateUrl: "/Angular/Views/manage.html"
        })
    $routeProvider.otherwise({ redirectTo: "/" });
});