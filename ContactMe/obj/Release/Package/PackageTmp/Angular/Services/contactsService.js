﻿var contactsService = ["$http", "$q", function ($http, $q) {
    
    var _contacts = [];
    var _contact = {};
    var _getContacts = function () {
        var deferred = $q.defer();

        $http.get("api/contacts").then(function (result) {
            angular.copy(result.data, _contacts);
            deferred.resolve();
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    var _getContact = function (id) {
        var deferred = $q.defer();

        $http.get("api/contacts/"+id).then(function (result) {
            result.data.loaded = true;
            angular.copy(result.data, _contact);
            deferred.resolve();
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    var _deleteContact = function (contact){
        var deferred = $q.defer();

        $http.delete("api/contacts/"+contact.id).then(function (result) {
            deferred.resolve();
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    var _saveContact = function (contact) {
        var deferred = $q.defer();

        $http.post("api/contacts/" + contact.id, contact).then(function (result) {
            deferred.resolve();
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    var _addContact = function (contact) {
        _contacts.push(contact);
        var deferred = $q.defer();

        $http.post("api/contacts", contact).then(function (result) {
            deferred.resolve();
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    var _search = function (searchStr) {
        var deferred = $q.defer();

        $http.post("api/search", searchStr).then(function (result) {
            angular.copy(result.data, _contacts);

            deferred.resolve();
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    return {
        contacts: _contacts,
        getContacts: _getContacts,
        contact: _contact,
        getContact: _getContact,
        deleteContact: _deleteContact,
        saveContact: _saveContact,
        addContact: _addContact,
        search: _search
    };
}];
app.factory("contactsService", contactsService);

