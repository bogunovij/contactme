﻿using ContactMe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ContactMe.Controllers
{
    public class ContactController : ApiController
    {
        IContactsRepository _repo;
        public ContactController(IContactsRepository repo)
        {
            _repo = repo;
        }
        public HttpResponseMessage Get(int id)
        {
            return this.Request.CreateResponse(HttpStatusCode.OK,
                _repo.GetContact(id));
        }
        public HttpResponseMessage Post([FromBody]Contact contact)
        {
            if (_repo.SaveContact(contact) && _repo.Save())
                return this.Request.CreateResponse(HttpStatusCode.OK);
            else
                return this.Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        public HttpResponseMessage Delete(int id)
        {
            if (_repo.DeleteContact(id) && _repo.Save())
                return this.Request.CreateResponse(HttpStatusCode.OK);
            else
                return this.Request.CreateResponse(HttpStatusCode.BadRequest);
        }

    }
}
