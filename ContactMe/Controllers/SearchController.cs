﻿using ContactMe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ContactMe.Models;

namespace ContactMe.Controllers
{
    public class SearchController : ApiController
    {
        IContactsRepository _repo;
        public SearchController(IContactsRepository repo)
        {
            _repo = repo;
        }
        public HttpResponseMessage Post([FromBody]StringObj searchObj)
        {   
            string searchStr = searchObj.Value;
            if (searchStr != null && searchStr.Length > 0)
            {
                var keywords = searchStr.Split(' ');
                return this.Request.CreateResponse(HttpStatusCode.OK,
                    _repo.SearchContacts(keywords));
            }
            else
            { 
                return this.Request.CreateResponse(HttpStatusCode.OK, _repo.GetAllContacts());
            }
        }
    }
}
