﻿using ContactMe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ContactMe.Controllers
{
    public class ContactsController : ApiController
    {
        IContactsRepository _repo;
        public ContactsController(IContactsRepository repo)
        {
            _repo = repo;
        }
        public HttpResponseMessage Get()
        {
            return this.Request.CreateResponse(HttpStatusCode.OK,
                _repo.GetAllContacts());
        }
        public HttpResponseMessage Post([FromBody]Contact contact)
        {
            if(_repo.AddContact(contact) && _repo.Save())
                return this.Request.CreateResponse(HttpStatusCode.Created);
            else
                return this.Request.CreateResponse(HttpStatusCode.BadRequest);
        }

    }
}
