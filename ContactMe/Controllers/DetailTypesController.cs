﻿using ContactMe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ContactMe.Controllers
{
    public class DetailTypesController : ApiController
    {
        IDetailTypesRepository _repo;
        public DetailTypesController(IDetailTypesRepository repo)
        {
            _repo = repo;
        }
        public HttpResponseMessage Get()
        {
            return this.Request.CreateResponse(HttpStatusCode.OK,
                _repo.GetDetailTypes());
        }
        public HttpResponseMessage Post([FromBody]DetailType type)
        {
            DetailType savedType;
            if (_repo.SaveDetailType(type, out savedType) && _repo.Save())
                return this.Request.CreateResponse(HttpStatusCode.OK, savedType);
            else
                return this.Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        public HttpResponseMessage Delete(int id)
        {
            if(_repo.DeleteDetailType(id) && _repo.Save())
                return this.Request.CreateResponse(HttpStatusCode.OK);
            else
                return this.Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
