﻿using ContactMe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ContactMe.Controllers
{
    public class TagsController : ApiController
    {
        ITagsRepository _repo;
        public TagsController(ITagsRepository repo)
        {
            _repo = repo;
        }
        public HttpResponseMessage Get()
        {
            return this.Request.CreateResponse(HttpStatusCode.OK,
                _repo.GetTags());
        }
    }
}
