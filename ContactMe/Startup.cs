﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ContactMe.Startup))]
namespace ContactMe
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
