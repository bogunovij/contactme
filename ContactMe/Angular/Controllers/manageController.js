﻿
var manageController = ["$scope", "$http", "$window", "detailTypesService", function ($scope, $http, $window, detailTypesService) {

    $scope.detailTypes = detailTypesService.detailTypes;
    detailTypesService.getDetailTypes().then(function () {
        $scope.detailTypesLoaded = true;
    }, function () {
        alert('An error occurred!');
    });

    $scope.hoverIn = function (detail) {
        detail.selected = true;
    };
    $scope.hoverOut = function (detail) {
        detail.selected = false;
    };
    $scope.addDetail = function () {
        var detail = { id: 0, name: "", placeholder: "", newlyCreated: true };
        $scope.detailTypes.push(detail);
    };
    $scope.saveDetail = function (detail, index) {
        detail.saving = true;
        detailTypesService.saveDetailType(detail).then(function (data) {
            $scope.detailTypes[index] = data;
        }, function () {
            detail.saving = false;
            alert('An error occurred!');
        });
    };
    $scope.deleteDetail = function (detail, index) {
        if(!detail.newlyCreated)
            detailTypesService.deleteDetailType(detail);
        $scope.detailTypes.splice(index, 1);
    };
    $scope.loading = function () {
        return !$scope.detailTypesLoaded;
    };
}];