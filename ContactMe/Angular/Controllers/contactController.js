﻿
var contactController = ["$scope", "$http", "$window", "$routeParams", "tagsService", "detailTypesService", "contactsService", function ($scope, $http, $window, $routeParams, tagsService, detailTypesService, contactsService) {

    $scope.detailTypes = detailTypesService.detailTypes;
    $scope.contact = contactsService.contact;
    detailTypesService.getDetailTypes().then(function () {
        $scope.detailTypesLoaded = true;
    }, function () {
        alert('An error has occurred!');
    });
    var assignInputChanged = function (tag){
        tag.inputChanged = function (str) {
            tag.value = str;
        };
        return tag;
    };
    contactsService.getContact($routeParams.id).then(function () {
        for (var i = 0; i < $scope.contact.tags.length; i++) {
            
            $scope.contact.tags[i] = assignInputChanged($scope.contact.tags[i]);
        }
    }, function() {
        alert('An error has occurred!');
    });
    $scope.tags = tagsService.tags;
    tagsService.getTags().then(function () {
        $scope.tagsLoaded = true;
    }, function () {
        alert('error');
    });
    $scope.addPhone = function () {
        var phone = { number: "", type: "Home" };
        $scope.contact.phones.push(phone);
    };
    $scope.addEmail = function () {
        var email = { address: "" };
        $scope.contact.emails.push(email);
    };
    $scope.removePhone = function (index) {
        $scope.contact.phones.splice(index, 1);
    };
    $scope.removeEmail = function (index) {
        $scope.contact.emails.splice(index, 1);
    };
    $scope.addTag = function () {
        var tag = {
            value: "", inputChanged: function (str) {
                tag.value = str;
            }
        };
        $scope.contact.tags.push(tag);
    };
    $scope.removeTag = function (index) {
        $scope.contact.tags.splice(index, 1);
    };

    $scope.deleteContact = function (contact) {
        $scope.busy = true;
        contactsService.deleteContact(contact).then(function () {
            $scope.busy = false;
            $window.location = "#/contacts";
        }, function () {
            alert('An error has occurred!');
            $scope.busy = false;
        });
    };
    $scope.saveContact = function (contact) {
        var found = false;
        for (var i = 0; i < $scope.contact.details.length; i++) {
            var detail = $scope.contact.details[i];
            if (detail.detailType.required && detail.value == "") {
                found = true;
            }
        }
        if (!found) {
            for (var i = 0; i < $scope.contact.tags.length; i++) {
                var tag = $scope.contact.tags[i];
                if (tag.obj && tag.obj.title.length > 0)
                    tag.value = tag.obj.title;
            }
            $scope.busy = true;
            contactsService.saveContact(contact).then(function () {
                $scope.busy = false;
                $window.location = "#/contacts";
            }, function () {
                alert('An error has occurred!');
                $scope.busy = false;
            });
        }
        else
            $scope.showRequired = true;
    };
    $scope.loading = function () {
        return !($scope.detailTypesLoaded && $scope.tagsLoaded) || $scope.busy;
    };
}];