﻿
var contactsController = ["$scope", "$http", "$window", "detailTypesService", "contactsService", function ($scope, $http, $window, detailTypesService, contactsService) {
    $scope.detailTypes = detailTypesService.detailTypes;
    $scope.contacts = contactsService.contacts;
    detailTypesService.getDetailTypes().then(function () {
        $scope.detailTypesLoaded = true;
    }, function () {
        alert('error');
    });
    contactsService.getContacts().then(function () {
        $scope.contactsLoaded = true;
    }, function () {
        alert('error');
    });
    $scope.hoverIn = function (contact) {
        contact.selected = true;
    };
    $scope.hoverOut = function (contact) {
        contact.selected = false;
    };
    $scope.deleteContact = function (index) {
        contactsService.deleteContact($scope.contacts[index]);
        $scope.contacts.splice(index,1);
    };
    $scope.viewContact = function (contact) {
        $window.location = "#/contacts/" + contact.id;
    };
    $scope.search = function (searchStr) {
        $scope.contactsLoaded = false;

        contactsService.search({value: searchStr}).then(function (result) {
            $scope.contactsLoaded = true;
        }, function () {
            alert('An error occurred');
        });
    };
    $scope.loading = function () {
        return !($scope.detailTypesLoaded && $scope.contactsLoaded);
    };
}];