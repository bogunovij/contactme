﻿var detailTypesService = ["$http", "$q", function ($http, $q) {
    var _detailTypes = [];
    var _getDetailTypes = function () {
        var deferred = $q.defer();

        $http.get("api/detailTypes").then(function (result) {
            angular.copy(result.data, _detailTypes);
            deferred.resolve();
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    var _saveDetailType = function (detail) {
        var deferred = $q.defer();

        $http.post("api/detailTypes", detail).then(function (result) {
            deferred.resolve(result.data);
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    var _deleteDetailType = function (detail) {
        var deferred = $q.defer();

        $http.delete("api/detailTypes/"+detail.id).then(function (result) {
            deferred.resolve();
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    return {
        detailTypes: _detailTypes,
        getDetailTypes: _getDetailTypes,
        saveDetailType: _saveDetailType,
        deleteDetailType: _deleteDetailType

    };
}];
 app.factory("detailTypesService", detailTypesService);