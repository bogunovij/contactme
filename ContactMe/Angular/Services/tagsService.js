﻿var tagsService = ["$http", "$q", function ($http, $q) {
    
    var _tags = [];
    var _getTags = function () {
        var deferred = $q.defer();

        $http.get("api/tags").then(function (result) {
            angular.copy(result.data, _tags);
            deferred.resolve();
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    };
    return {
        tags: _tags,
        getTags: _getTags
    };
}];
app.factory("tagsService", tagsService);

